package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	private static final String BRANCH_CODE = "^[0-9]{3}$";
	private static final String BRANCH_FILE_TYPE = "支店定義";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODUTY_LST = "commodity.lst";
	private static final String COMMODITY_CODE = "^[A-Za-z0-9]{8}$";
	private static final String COMMODITY_FILE_TYPE = "商品定義";
	
	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "ファイルのフォーマットが不正です";
	private static final String NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String OVER_DIGITS = "合計金額が10桁を超えました";
	private static final String NOT_BRANCHCODE_FORMAT = "の支店コードが不正です";
	private static final String NOT_SALESDATA_FORMAT = "のフォーマットが不正です";
	private static final String NOT_COMMODITYCODE_FORMAT = "の商品コードが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//コマンドライン引数が渡されているか確認（エラー処理3）
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();


		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales,BRANCH_CODE, BRANCH_FILE_TYPE)) {

			return;
		}


		// 商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODUTY_LST, commodityNames, commoditySales ,COMMODITY_CODE, COMMODITY_FILE_TYPE)) {
			return;
		}
		

		// パスに存在する全てのファイルの情報を格納します(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		//ファイルの情報を格納するList(ArrayList)を宣言します
		List<File> rcdFiles = new ArrayList<>();
		//filesの数だけ繰り返します（指定したパスに存在するファイルの数だけ）
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);

			}
		}

		//売上ファイルが連番になっているか確認（エラー処理2-1）
		Collections.sort(rcdFiles);
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(NOT_SERIAL_NUMBER);
				return;
			}
		}

		//rcdFilesに格納されたファイルの数だけ繰り返します
		for (int i = 0; i < rcdFiles.size(); i++) {

			//売上ファイルを読み取ります（2-2)
			BufferedReader br = null;
			String line;
			//売上ファイルの中身を格納するリストを宣言します
			List<String> salesData = new ArrayList<String>();
			try {
				File file = rcdFiles.get(i);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				//指定されたファイル内でnullになるまで行を読み取り、salesdataへ格納します
				while ((line = br.readLine()) != null) {
					salesData.add(line);

				}

				//売上ファイルのフォーマットを確認します（エラー処理2）
				if (salesData.size() != 3) {
					System.out.println(file.getName() + NOT_SALESDATA_FORMAT);
					return;
				}

				//売上ファイル内の支店コードが支店定義ファイルに存在するか確認（エラー処理2）
				if (!branchNames.containsKey(salesData.get(0))) {
					System.out.println(file.getName() + NOT_BRANCHCODE_FORMAT);
					return;
				}
				
				//売上ファイル内の商品コードが商品定義ファイルに存在するか確認
				if (!commodityNames.containsKey(salesData.get(1))) {
					System.out.println(file.getName() + NOT_COMMODITYCODE_FORMAT);
					return;
				}

				//売上金額が数字か確認（エラー処理3）
				if (!salesData.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;

				}

				//売上金額の型を変換します String→long
				long fileSale = Long.parseLong(salesData.get(2));

				//branchSalesから売上金額を抽出し、salesDataの売上金額を足して、branchSalesに戻します。
				Long saleAmount = branchSales.get(salesData.get(0)) + fileSale;
				//commoditySalesから売上金額を抽出し、salesDataの売上金額を足して、commoditySalesに戻します。
				Long commoditySaleAmount = commoditySales.get(salesData.get(1)) + fileSale;
				//支店別、商品別それぞれの売上金額の合計が10桁を超えていないか確認（エラー処理2）
				if (saleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
					System.out.println(OVER_DIGITS);
					return;
				}
				branchSales.put(salesData.get(0), saleAmount);
				commoditySales.put(salesData.get(1), commoditySaleAmount);
				
			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;

		}
		//商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル・商品定義ファイルの読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap もしくは商品コードと商品名を保持するMap
	 * @param 支店コードと売上金額を保持するMap もしくは商品コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> mapNames,
			Map<String, Long> mapSales , String lst, String fileType) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//ファイルの存在確認（エラー処理1）
			if (!file.exists()) {
				System.out.println(fileType + FILE_NOT_EXIST);
				return false;

			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				// 支店コードと支店名を「,」で区切る(処理内容1-2)
				String[] items = line.split(",");

				//ファイルのフォーマット確認 支店定義ファイル・商品定義ファイル


				if ((items.length != 2 || (!items[0].matches(lst)))){

					System.out.println(fileType + FILE_INVALID_FORMAT);
					return false;

				}


				//支店コードと支店名をbranchNamesに値を保持する
				mapNames.put(items[0], items[1]);
				//支店コードと売上金額（0円）をbranchSalesに値を保持する
				mapSales.put(items[0], 0L);

			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}


	/**
	 * 支店別集計ファイル・商品別集計ファイルの書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap もしくは商品コードと商品名を保持するMap
	 * @param 支店コードと売上金額を保持するMap もしくは商品コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */

	private static boolean writeFile(String path, String fileName, Map<String, String> mapNames,
			Map<String, Long> mapSales) {
		// 書き込み処理(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fr = new FileWriter(file);
			bw = new BufferedWriter(fr);

			for (String key : mapSales.keySet()) {
				bw.write(key + "," + mapNames.get(key) + "," + mapSales.get(key));

				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
